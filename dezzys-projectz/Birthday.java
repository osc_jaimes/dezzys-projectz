package birthday;

import java.util.Scanner;

public class Birthday {

    public static void main(String[] args) throws InterruptedException {

        String[] names = new String[5];
        String[] birthDay = new String[5];
        String skipLines = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";

        String today = "";
        
        //checking todays date
        

        Scanner sc = new Scanner(System.in);
        System.out.println("if you want to quit while entering the names just press 'x' then enter. ");
        
        //get names and birthdays using scanner and for-loop
        for (int i = 0; i < 5; i++) {
            if(i == 0){
                 
        System.out.println("Please enter todays today in the form of monthday i.e(228 for Feb 28)");
        
            today = sc.nextLine();
            }
            
            System.out.println("Enter the name of person #" + (i + 1) + ":");

            names[i] = sc.nextLine();
            //check to see if x was pressed
            if (names[i].equals("x")) {
                System.exit(0);
            }
            System.out.println("Enter the birthdayday of persion #" + i + "in the form of monthday i.e(228 for Feb 28)");

            birthDay[i] = sc.nextLine();
            //check to see if x was pressed
            if (birthDay[i].equals("x")) {
                System.exit(0);
            }
        }
        sc.close();
        //making room in console
        System.out.println(skipLines);

        //check birthdays and compare them from greatest to least
        //in terms of birthdays in the year, the bigger the date,
        //the younger one is.
        boolean swapped = true;
        int j = 0;
        String temp;
        String tempName;
        while (swapped) {
            //make swapped false so if the loop does not run,
            //it will not run again.
            swapped = false;
            j++;
            for (int i = 0; i < 5 - j; i++) {
                if (Integer.valueOf(birthDay[i]) < Integer.valueOf(birthDay[i + 1])) {
                    //temporary variable to hold the element at the nth place
                    temp = birthDay[i];
                    //swap the n and n+1
                    //utilizing the temporary variable
                    //swapped both arrays at the same time
                    //did this using the same variable 'i' defined in the for-loop because
                    //they are paralle arrays that reference to "linked items"
                    tempName = names[i];
                    birthDay[i] = birthDay[i + 1];
                    names[i] = names[i + 1];
                    birthDay[i + 1] = temp;
                    names[i + 1] = tempName;
                    //make swapped true so the loop can conitnue
                    //and keep checking;
                    swapped = true;

                }
            }

        }
        //dont forget to close scanner.
        //very important
        sc.close();

        System.out.println("Birthdays and names in order from youngest to oldest are:");
        for (int i = 0; i < 5; i++) {
            System.out.println(names[i] + ", " + Integer.valueOf(birthDay[i]));
        }
        
        //pausing the program so the user can read the results
        Thread.sleep(3000);

      
        System.out.print("\n\n");
        
        //printing out the birthdays from the given dat
        System.out.println("birthdays from todays date, in order, are:");
        for (int i = 0; i < 5; i++) {
            //comparing the values of birthdays to the value of the day today
            if (Integer.valueOf(birthDay[i])
                    >= Integer.valueOf(today)) {
                System.out.println(names[i] + ", " + Integer.valueOf(birthDay[i]));
            }

        }
    }

}


